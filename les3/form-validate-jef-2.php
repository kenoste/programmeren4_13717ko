<?php
    $productError = '';
    $priceError = '';
    if (isset($_POST['product'])) {
        $productName = $_POST['product'];
        if (empty($productName)) {
            $productError = 'Product moet een naam hebben!';
            $productPattern = '/^[a-zA-Zéç ]*$/';
        }
        if (!preg_match($productPattern, $productName)) {
            $productError = 'Naam bestaat uit karakters alleen!';
        }

    }
    if (isset($_POST['price'])) {
    $price = $_POST['price'];
    if (empty($price)) {
        $productError = 'Product moet een prijs hebben!';
        $pricePattern = '/^[0-9]+(\.[0-9]{1,2})?$/';
        
        if (!preg_match($pricePattern, $price)) {
            $priceError = 'Typ een geldig eurogetal in! Bv. 99.00';
        }
    }
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Formulier validatie</title>
</head>
<body>
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
            <label for="product"></label>
            <input id="product" name="product" type="text" required>
            <span><?php echo $productError;?></span>
            <label for="price"></label>
            <input id="price" name="price" type="text" required pattern="^[0-9]+(\.[0-9]{1,2})?$">
            <span><?php echo $priceError;?></span>
            
            
            <button type=submit>Verzenden</button>
        </form>
</body>
</html>

<!-- 
^                   # Start of string.
[0-9]+              # Must have one or more numbers.
(                   # Begin optional group.
    \.              # The decimal point, . must be escaped, 
                    # or it is treated as "any character".
    [0-9]{1,2}      # One or two numbers.
)?                  # End group, signify it's optional with ?
$                   # End of string.
-->