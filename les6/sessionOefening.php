<?php

$page = htmlspecialchars($_SERVER['PHP_SELF']);

session_set_cookie_params(3600 * 24 * 7);
session_start();

function getProductText($product)
{
    switch ($product) {
            case 0 :
                 $artikelText = 'GSM';
                break;
            case 1 :
                $artikelText = 'Tablet';
                break;        
            case 2 :
                $artikelText = 'Pc';
                break;
            case 3 :
                $artikelText = 'Ereader';
                break;
            case 4 :
                $artikelText = 'Auto';
                break;
            case 5 :
                $artikelText = 'Brommer';
                break;
            case 6 :
                $artikelText = 'Fiets';
                break;
            case 7 :
                $artikelText = 'Ereader';
                break;
            default :
                $artikelText = 'Skatebord';
        }
        
        return $artikelText; 
}


//////////////////////////////////////////////////////////////////////
if(isset($_POST['product'])) {
    if(isset($_SESSION['cart'])) {
        $cart = unserialize($_SESSION['cart']);
    }
    
    $cart[] = getProductText($_POST['product']);
    
    $_SESSION['cart'] = serialize($cart);
    header("Location: " . $page);
    
}
//////////////////////////////////////////////////////////////////////
if(isset($_GET['remove'])) {
    if(isset($_SESSION['cart'])) {
        $cart = unserialize($_SESSION['cart']);
    }
    
    unset($cart[$_GET['remove']]);
    $_SESSION['cart'] = serialize($cart);
    header("Location: " . $page);
}
//////////////////////////////////////////////////////////////////////

if(isset($_GET['empty'])) {
   session_unset();
   header("Location: " . $page);
    
}
//////////////////////////////////////////////////////////////////////
?>


<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Werken met sessies</title>
    <style>
        table, th, td{
            border-collapse: collapse;
            border:1px solid black;
            width:200px;
        }
        
        tr{
            background-color:white ;
        }
        
        tr:hover {
            background-color:Snow ;
        }
        
        Fieldset {
            background-color: 	#E0EAF4;
            width: 500px;    
            border-color: #f9ede0;
            margin: 10px;
        }

    </style>
</head>
<body>
    <h1>Mijn shoppingcart</h1>
    <fieldset>
        <legend>Bestellen</legend>
    <form action="<?php echo $page;?>" method="post">
        <label for="product">Producten:</label>
        <select name="product" id="product">
            <option value="0">GSM</option>
            <option value="1">Tablet</option>
            <option value="2">Pc</option>
            <option value="3">Ereader</option>
            <option value="4">Auto</option>
            <option value="5">Brommer</option>
            <option value="6">Fiets</option>
            <option value="7">Skatebord</option>
        </select>
        <button type="submit">Bestel</button>
    </form>
    <br/>
    <div>
        <?php
        if(isset($_SESSION['cart'])) {
            $cart = unserialize($_SESSION['cart']);
            echo "<table>";
            foreach ($cart as $key => $value) {
                echo "<tr>";
                echo '<td>' . $value.'</td>';
                echo '<td><a href="' . $page . '?remove='.$key.'">Verwijder</a></td>';
                echo "</tr>";
            }
             echo "</table>";
             echo "<br/>";
            echo '<a href="' . $page . '?empty=all">Verwijder alles</a>';
        }else{
            echo 'Je cart is leeg.';
        }
        ?>
    </div>
    </fieldset>
</body>
</html>