<?php
    include ( __DIR__ . '/vendor/autoload.php');
    $appState = new \ModernWays\Dialog\Model\NoticeBoard();
    $request = new \ModernWays\Mvc\Request('/home/index2');
    $route = new \ModernWays\Mvc\Route($appState, $request->uc());
   
    // de namespace waarin de klassen staan van mijn app/project.
    // In de psr4 autoload moet ik dan het pad opgeven waar de klassen van die namespace staan
    //
    // de volgende methode maakt een instantie van de klasse home en voert de methode index van die 
    // klasse uit op voorwaardedat er geen andere route wordt meegegeven
    $routeConfig = new \ModernWays\Mvc\RouteConfig('\Programmeren4\Les9', $route, $appState);

    // met invokeActionMethod voeren we de methode uit
    $view = $routeConfig->invokeActionMethod();
    
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MVC webapp</title>
</head>
<body>
    <?php
        $view();
    ?>
</body>
</html>