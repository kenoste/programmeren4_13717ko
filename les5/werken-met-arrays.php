<?php
    $persons = array (
        array('Johanna', 'Den Doper', 117, 'Vrouw'),
        array('Mohamed', 'El Farisi', 40, 'Man')
    );
    
    $personsAssoc = array (
        array('FirstName' => 'Johanna', 'FamilyName' => 'Den Doper', 'Age' => 117, 'Gender' => 'Vrouw'),
        array('FirstName' => 'Mohamed', 'FamilyName' => 'El Farisi', 'Age' => 40, 'Gender' => 'Man')
    );
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Werken met arrays in PHP</title>
</head>
<body>
    <p>Genummerde indexen</p>
    <table>
        <thead>
        <tr>
            <th>Voornaam</th>
            <th>Familienaam</th>
            <th>Leeftijd</th>
            <th>Gender</th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach ($persons as $person) { ?>
                <tr>
                    <td><?php echo $person[0];?></td>
                    <td><?php echo $person[1];?></td>
                    <td><?php echo $person[2];?></td>
                    <td><?php echo $person[3];?></td>
                </tr>
            <?php 
            } ?>
            </tbody>
    </table>
    <p>String indexen</p>
    <table>
        <thead>
        <tr>
            <th>Voornaam</th>
            <th>Familienaam</th>
            <th>Leeftijd</th>
            <th>Gender</th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach ($personsAssoc as $person) { ?>
                <tr>
                    <td><?php echo $person['FirstName'];?></td>
                    <td><?php echo $person['FamilyName'];?></td>
                    <td><?php echo $person['Age'];?></td>
                    <td><?php echo $person['Gender'];?></td>
                </tr>
            <?php 
            } ?>
            </tbody>
    </table>
</body>
</html>