


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Belgische postcodes</title>
    <style>
    /*
        table tr:nth-child(3n+1) {
            background-color: black;
        }
        table tr:nth-child(3n+2) {
            background-color: yellow;
        }
        table tr:nth-child(3n) {
            background-color: red;
        }
    */
    </style>
</head>
<body>
    <table border=1>
   
    <?php
    
    $fileContent = file_get_contents("data/Postcodes.csv");
    
    $fileContent= utf8_encode($fileContent);
    
    $fileLines= explode("\n", $fileContent);
    
    foreach ($fileLines as $line) {
        echo "<tr>";
        
        $colls = explode("|", $line);
        foreach ($colls as $coll) {
            echo "<td>" . $coll . "</td>";
        }
        
        echo "</tr>";
    }
        
    ?>
   
    </table>    
</body>

</html>