<?php
namespace Programmeren4\Les10\Controller;

class PostalCode extends \ModernWays\Mvc\Controller
{
    public function show()
    {
        $textPostcalCodes = file_get_contents("vendor/programmeren4/les10/data/Postcodes.csv");
        $textPostcalCodes= utf8_encode($textPostcalCodes);
        $lines = explode("\n", $textPostcalCodes);
        
        $model = new \Programmeren4\Les10\Model\PostalCode();
        
       
        $postalCodes = array();
        foreach ($lines as $line) {
            $postalCodes[] =  explode('|', $line);
        }
        
        $model->setList($postalCodes);
        return $this->view('PostalCode','Show', $model);
        
        
        //return $model->setList($lines);
        
    }
}