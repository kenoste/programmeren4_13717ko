<?php
namespace Programmeren4\Les10\Controller;


class Home extends \ModernWays\Mvc\Controller
{
    public function index()
    {
      
        $model = new \Programmeren4\Les10\Model\MyModel();
        
        $fileContent = file_get_contents("vendor/programmeren4/les10/data/Postcodes.csv");
        $fileContent= utf8_encode($fileContent);
        
        $model->fullfileString = $fileContent;
        
        return $this->view('Home','Index', $model);
    }
    
   
}


