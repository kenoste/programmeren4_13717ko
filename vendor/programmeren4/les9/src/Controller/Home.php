<?php
namespace Programmeren4\Les9\Controller;



class Home extends \ModernWays\Mvc\Controller
{
    public function index()
    {
        //$model = 'Dat is 3 penny MVC';
        $model = new \Programmeren4\Les9\Model\MyModel();
        $model->naam = "Oste";
        $model->voornaam = "Ken";
       
        return $this->view('Home','Index', $model);
    }
    
}


/*

eerst request klasse
dan route die de url parsed
routeconfig start controller object en voert actie uit, 
deze maakt dan de model, vult deze, en geeft deze door aan de view.

updates (van mvc picture) => toont de view (bv in test-3p-mvc)


*/