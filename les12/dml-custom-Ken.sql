use kenoste;
DROP PROCEDURE IF EXISTS ArticleSelectAll;
DELIMITER //
CREATE PROCEDURE ArticleSelectAll()
BEGIN
SELECT `Article`.`Id`,
	`Article`.`Name`,
	`Article`.`PurchaseDate`,
	`Article`.`Price`
	FROM `Article`
	ORDER BY `Name`;
END //
DELIMITER ;

-- Stored Procedure
-- Article
-- Select row based on Id

use kenoste;
DROP PROCEDURE IF EXISTS ArticleSelectOne;
DELIMITER //
CREATE PROCEDURE ArticleSelectOne(
	pId int
)
BEGIN
SELECT `Article`.`Id`,
	`Article`.`Name`,
	`Article`.`PurchaseDate`,
	`Article`.`Price`
	FROM `Article`
	WHERE pId = `Article`.`Id`
	ORDER BY `Name`;
END //
DELIMITER ;


use kenoste;
call ArticleInsert('Mercedes', '2016-12-15 12:37:30', '40.50', @pId);
select @pId;